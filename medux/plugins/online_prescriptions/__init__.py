# For plugin apps, the AppConfig is found automatically.
# default_app_config = (
#     "medux.plugins.online_prescriptions.apps.OnlinePrescriptionsConfig"
# )

__version__ = "0.0.1"
__license__ = "AGPLv3+"
