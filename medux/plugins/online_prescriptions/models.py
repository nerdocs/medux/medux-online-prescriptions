import secrets
import string

from django.contrib.auth.hashers import make_password
from django.db import models

from medux.core.models import Patient as MeduxPatient


def create_random_password(digits=6):
    alphabet = string.ascii_letters + string.digits
    return "".join(secrets.choice(alphabet) for i in range(digits))


class Patient(MeduxPatient):
    """A Patient proxy model that stores an online id and a password hash of a password."""

    online_id = models.UUIDField()
    online_password = models.CharField(max_length=255)


# make_password()
